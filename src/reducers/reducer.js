

const updateCartItems = (cartItems, item, idx) => {

    if (item.count === 0) {
      return [
        ...cartItems.slice(0, idx),
        ...cartItems.slice(idx + 1)
      ];
    }
  
    if (idx === -1) {
      return [
        ...cartItems,
        item
      ];
    }
  
    return [
      ...cartItems.slice(0, idx),
      item,
      ...cartItems.slice(idx + 1)
    ];
  };
  
  const updateCartItem = (book, item = {}, quantity) => {
  
    const {
      id = book.id,
      count = 0,
      title = book.title,
      total = 0 } = item;
  
    return {
      id,
      title,
      count: count + quantity,
      total: total + quantity*book.price
    };
  };
  
  const updateOrder = (state, bookId, quantity) => {
    const { books, cartItems } = state;
  
    const book = books.find(({id}) => id === bookId);
    const itemIndex = cartItems.findIndex(({id}) => id === bookId);
    const item = cartItems[itemIndex];
  
    const newItem = updateCartItem(book, item, quantity);
    return {
      ...state,
      cartItems: updateCartItems(cartItems, newItem, itemIndex)
    };
  };
                
const initialState = {
    books : [],
    loading : true,
    error : null,
    cartItems : []
}
const reducer = (state = initialState, actions) => {

    switch(actions.type){
        case 'BOOKS_REQUEST' :
            return {
                ...state,
                books : [],
                loading : true,
                error : null
            };
        case 'BOOKS_LOADED' :
            return {
                ...state,
                books : actions.payload,
                loading : false,
                error : null
            };
        case 'BOOKS_ERROR' :
            return {
                ...state,
                books : [],
                loading : false,
                error : actions.payload
            };    
        case 'ADD_TO_CART' :
            return updateOrder(state, actions.payload, 1);
        
        case 'DECREASE' :
            return updateOrder(state, actions.payload, -1);
            
        case 'DELETE' :
            const item = state.cartItems.find(({id}) => id === actions.payload);
            return updateOrder(state, actions.payload, -item.count);

            
        
        default :
            return state;
    }

}

export default reducer;