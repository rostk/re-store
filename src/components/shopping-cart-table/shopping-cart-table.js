import React from 'react';
import './shopping-cart-table.css';
import {connect} from 'react-redux';
import { onIncrease, onDecrease, onDelete } from '../../actions';
const ShoppingCartTable = ({items, onIncrease, onDecrease, onDelete}) => {
  const listItems = (item, idx) =>{
    const {id, title, count, total} = item;
    return (
    <tr key={id}>
      <td>{idx + 1 }</td>
      <td>{title}</td>
      <td>{count}</td>
      <td>${total}</td>
      <td className='btns'>
    <button className="btn btn-outline-danger btn-sm float-right" onClick={()=>onDelete(id)}>
      <i className="fa fa-trash-o" />
    </button>
    <button className="btn btn-outline-success btn-sm float-right" onClick={()=>onIncrease(id)}>
      <i className="fa fa-plus-circle" />
    </button>
    <button className="btn btn-outline-warning btn-sm float-right" onClick={()=>onDecrease(id)}>
      <i className="fa fa-minus-circle" />
    </button>
    </td>
  </tr>
    )
  }
  return (
    <div className="shopping-cart-table">
      <h2>Your Order</h2>
      <table className="table">
        <thead>
          <tr>
          <th>#</th>
          <th>Item</th>
          <th>Count</th>
          <th>Price</th>
          <th>Action</th>
          </tr>
        </thead>

        <tbody>
          {
              items.map(listItems)
            }
          
        </tbody>
      </table>

      <div className="total">
        Total: $201
      </div>
    </div>
  );
};
const mapStateToProps = ({cartItems}) =>{
return {
  items : cartItems
};
}

const mapDispatchToProps = (dispatch) => {
return {
  onIncrease : (id)=> dispatch(onIncrease(id)),
  onDecrease : (id)=> dispatch(onDecrease(id)),
  onDelete : (id)=> dispatch(onDelete(id))
}
}


export default connect(mapStateToProps,mapDispatchToProps)(ShoppingCartTable);
