import React, { Component } from 'react';
import BookListItem from '../book-list-item';
import { connect } from 'react-redux';
import ErrorIndicator from '../error-indicator'
import { withBookstoreService } from '../hoc';
import { fetchBooks, addToCart } from '../../actions';
import { compose } from '../../utils';
import Spinner from '../spinner'
import './book-list.css';

class BookListContainer extends Component {

  componentDidMount() {
    const { fetchBooks } = this.props;
    fetchBooks();
  }

  render() {
    const { books, loading, error, addToCart } = this.props;
    // console.log(loading);
    if(loading){
      return <Spinner />
    }
    if(error){
      return <ErrorIndicator />
    }
    return (
      <BookList books={books} addToCart={addToCart}/>
    );
  }
}


const BookList = ({books, addToCart}) => {
  return (
    <ul className="book-list">
      {
        books.map((book) => {
          return (
            <li key={book.id}><BookListItem book={book} addToCart={()=>addToCart(book.id)}/></li>
          )
        })
      }
    </ul>
  );
}

const mapStateToProps = ({ books, loading, error }) => {
  
  return { books, loading, error };
};

const mapDispatchToProps = ( dispatch, ownProps) => {
  const {bookstoreService } = ownProps;
  return {
    fetchBooks : fetchBooks(bookstoreService, dispatch),
    addToCart : (id)=> dispatch(addToCart(id))
  }
  
};

export default compose(
  withBookstoreService(),
  connect(mapStateToProps, mapDispatchToProps)
)(BookListContainer);
