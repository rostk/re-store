

const booksLoaded = (newBooks) => {
    return {
        type : 'BOOKS_LOADED',
        payload : newBooks
    }
}

const booksRequest = () =>{
    return {
        type : 'BOOKS_REQUEST'
    }
}
const booksOnError = (error) =>{
    return {
        type : 'BOOKS_ERROR',
        payload : error
    }
}
const addToCart = (bookId) => {
    return {
        type : 'ADD_TO_CART',
        payload : bookId
    }
}

const onIncrease = (id) =>{
    return {
        type: 'ADD_TO_CART',
        payload: id
    }
}
const onDecrease = (id) =>{
    return {
        type: 'DECREASE',
        payload: id
    }
}
const onDelete= (id) =>{
    return {
        type: 'DELETE',
        payload: id
    }
}

const fetchBooks = (bookstoreService, dispatch) => () => {
    dispatch(booksRequest());
    bookstoreService.getBooks()
      .then((data)=> dispatch(booksLoaded(data)))
      .catch((e)=> dispatch(booksOnError(e)))
}


export { fetchBooks, addToCart, onIncrease, onDecrease, onDelete };